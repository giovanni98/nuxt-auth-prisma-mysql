// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  css: ['~/assets/css/main.scss'],
  modules: [
    'nuxt-icon',
    '@sidebase/nuxt-auth'
  ],
  runtimeConfig: {
    authSecret: '',
    githubId: '',
    githubSecret: '',
    googleId: '',
    googleSecret: '',
    authOrigin: '',
    public: {

    }
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
})